#include "Objet.h"
#include <Eigen/Dense>

void Objet::updatePosition(int t)
{
	Eigen::MatrixXf tmp(2,1);
	for (int i = 0; i < t; i++) {
		tmp << _x + _vx * i,
			   _y + _vy * i;
		Eigen::MatrixXf oldpos = *_positions;
		_positions->resize(_positions->rows(), _positions->cols() + 1);
		*_positions << oldpos, tmp;
	}
}