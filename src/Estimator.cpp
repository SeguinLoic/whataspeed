#include "Estimator.h"
#include <Eigen/Dense>
#include <iostream>
#include <cmath>

/**
 * Calculer la matrice "C" à partir des angles calculés.
 *
 * @param angles Les angles calculés par le simulateur
 * @return La matrice "C"
 */
Eigen::MatrixXf Estimator::C_Matrix(std::vector<double> angles)
{
	Eigen::MatrixXf C(angles.size(), 4);
	double theta;

	for (int i = 0; i < angles.size(); i++)
	{
		theta = angles.at(i);
		C(i,0) = sin(theta);
		C(i,1) = -cos(theta);
		C(i,2) = i * sin(theta);
		C(i,3) = -i * cos(theta);
	}

	return C;
}

/**
 * Calculer la matrice "Y" à partir des angles calculés et de la position de l'observateur.
 *
 * @param angles Les angles calculés par le simulateur
 * @param positions Les positions de l'observateur
 * @return La matrice "Y"
 */
Eigen::MatrixXf Estimator::Y_Matrix(std::vector<double> angles, Eigen::MatrixXf positions)
{
	Eigen::MatrixXf Y(angles.size(), 1);
	double theta;

	for (int i = 0; i < angles.size(); i++)
	{
		theta = angles.at(i);
		Y(i,0) = positions(0,i) * sin(theta) - positions(1,i) * cos(theta);
	}

	return Y;
}

/**
 * Retrouver en utilisant la méthode inverse la matrice X,
 * solution du problème, dont les composantes sont les
 * valeurs qui étaient à déterminer.
 *
 * @param C La matrice C
 * @param Y La matrice Y
 * @return La matrice X, solution du problème
 */
Eigen::MatrixXf Estimator::inverse(Eigen::MatrixXf C, Eigen::MatrixXf Y)
{
	Eigen::MatrixXf CtC = C.transpose() * C;
	Eigen::MatrixXf CtY = C.transpose() * Y;
	Eigen::MatrixXf result = CtC.inverse() * CtY;
	return result;
}


/**
 * Retrouver en utilisant la méthode du gradient conjugué
 * la matrice X, solution du problème, dont les composantes
 * sont les valeurs qui étaient à déterminer.
 *
 * @param C La matrice C
 * @param Y La matrice Y
 * @return La matrice X, solution du problème
 */
Eigen::MatrixXf Estimator::conjugate_gradient(Eigen::MatrixXf C, Eigen::MatrixXf Y)
{
	Eigen::MatrixXf resultt(4,1);

	Eigen::MatrixXf CtC = C.transpose() * C;
	Eigen::MatrixXf CtY = C.transpose() * Y;

	Eigen::MatrixXf g = CtC * resultt - CtY;

	Eigen::MatrixXf h = g;

	for (int i = 0; i < resultt.rows(); i++)
	{
		double tmp = ((h.transpose() * g)(0,0) / (h.transpose() * CtC * h)(0,0));
		resultt = resultt - g * tmp;
		g = g - CtC * h * tmp;
		h = g - h * (h.transpose() * CtC * g)(0,0) / (h.transpose() * CtC * h)(0,0);
	}

	return resultt;
}

void Estimator::prediction_error(Eigen::MatrixXf computed, Eigen::MatrixXf theoric)
{
	for (int i = 0; i < computed.rows(); i++)
	{
		for (int j = 0; j < computed.cols(); j++)
		{
			std::cout << std::abs(computed(i,j) - theoric(i,j)) << "\n";
		}
	}
}

/**
 * Retrouver en utilisant la méthode des moindres carrés
 * récursifs la matrice X, solution du problème, dont les
 * composantes sont les valeurs qui étaient à déterminer.
 *
 * @param T Le nombre maximum de périodes
 * @param C La matrice C
 * @param Y La matrice Y
 * @return La matrice X, solution du problème
 */
Eigen::MatrixXf Estimator::recursive(int T, Eigen::MatrixXf C, Eigen::MatrixXf Y)
{
	Eigen::MatrixXf result(4,1);
	Eigen::MatrixXf C2(4,1);
	Eigen::MatrixXf P(4,4);

	for (int i = 0; i < 4; i++)
	{
		P(i,i) = 100000;
	}

	for (int i = 0; i < T; i++)
	{
		C2(0,0) = C(i,0);
		C2(1,0) = C(i,1);
		C2(2,0) = C(i,2);
		C2(3,0) = C(i,3);

		double prediction_error = Y(i,0) - ((C2.transpose() * result)(0,0));

		double tmp = ((C2.transpose() * P) * C2)(0,0);

		Eigen::MatrixXf k = (P * C2) * (1 / (tmp + 1));

		result += (k * prediction_error);

		P -= (k * C2.transpose() * P);
	}

	return result;
}