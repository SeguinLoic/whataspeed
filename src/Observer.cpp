#include <cmath>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "Observer.h"

void Observer::updatePosition(int t)
{
	Eigen::MatrixXf tmp(2,1);
	Eigen::MatrixXf oldpos;
	for (int i = 0; i < t; i++) {
		double new_x_theoric = (double) (_x + _rayon * cos(_w0 * i));
		double new_y_theoric = (double) (_y + _rayon * sin(_w0 * i));
		double new_x_noise = new_x_theoric + noise();
		double new_y_noise = new_y_theoric + noise();

		tmp << new_x_noise,
			   new_y_noise;
		oldpos = *_noise;
		_noise->resize(_noise->rows(), _noise->cols() + 1);
		*_noise << oldpos, tmp;

		tmp << new_x_theoric,
			   new_y_theoric;
		oldpos = *_positions;
		_positions->resize(_positions->rows(), _positions->cols() + 1);
		*_positions << oldpos, tmp;
	}
}

/**
 * Calcule le bruit à appliquer à la position de l'utilisateur.
 * @return Un bruit contenu entre -0,5 et 0,5 de moyenne 0.
 */
double Observer::noise()
{
	double random = ((double)rand() / (double)RAND_MAX);
	return random - 0.5;
}