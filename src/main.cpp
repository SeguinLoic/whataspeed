#include <iostream>
#include <string>
#include "gnuplot_i.hpp"

#include "Objet.h"
#include "Observer.h"
#include "Simulator.h"
#include "Estimator.h"
#include <Eigen/Dense>

#define BLUE "\033[34m"
#define NORMAL "\033[0m"
#define RED "\e[0;31m"

#define DEBUG 0

int debug_index = 0;

void debug() {
	if (DEBUG != 0)  
		std::cout << RED << "** DEBUG " << debug_index++
						 << " **" << NORMAL << std::endl;
}

int main(int argc, char** argv) {

	int NBUPDATE_O = 20;
	int NBUPDATE_OBS = 20;

	if (argc == 3)
	{
		NBUPDATE_O = atoi(argv[1]);
		NBUPDATE_OBS = atoi(argv[2]);
	}

	Objet o(5,5,2,2.5);
	Observer obs(20,20,0.2,50);
	o.updatePosition(NBUPDATE_O);
	obs.updatePosition(NBUPDATE_OBS);


for (int i = 0; i < 2; i++) {

	if (i == 0) {
		std::cout << RED << ">>>>>>> THEORIQUE <<<<<<<" << NORMAL << std::endl;
	} else {
		std::cout << RED << ">>>>>>> AVEC BRUIT <<<<<<<" << NORMAL << std::endl;
	}

	std::cout << BLUE << "*** o.positions() ***" << NORMAL << std::endl;
	std::cout << *(o.positions()) << std::endl;

debug();

	std::cout << BLUE << "\n*** obs.positions() ***" << NORMAL << std::endl;

	if (i == 0) {
		std::cout << *(obs.positions()) << std::endl;
	} else {
		std::cout << *(obs.noise_positions()) << std::endl;
	}

debug();

	Gnuplot g1("lines");
	g1 << "set terminal aqua title \"SI4 - Estimation récursive des paramètres\"";
	if (i == 0) {
		g1.set_title("Simulateur sans bruit");
	} else {
		g1.set_title("Simulateur avec bruit");
	}
	g1.set_xrange(-40,80);
	g1.set_yrange(-40,80);
	g1.plot_slope(1,2,"1,2");

debug();

	g1 << "plot '-' using 1:2 title \"Observateur\" with lp, \
     '-' using 1:2 title \"Mobile\" with lp";

debug();
	
	if (i == 0) {
    	for (int i=0; i < NBUPDATE_OBS; i++) {
			g1 << std::to_string(obs.x(i)) + " " + std::to_string(obs.y(i)) + "\n";
		}
	} else {
		for (int i=0; i < NBUPDATE_OBS; i++) {
			g1 << std::to_string(obs.x_noise(i)) + " " + std::to_string(obs.y_noise(i)) + "\n";
		}
	}

debug();

	g1 << "EOF";
    for (int i=0; i < NBUPDATE_O; i++) {
		g1 << std::to_string(o.x(i)) + " " + std::to_string(o.y(i)) + "\n";
	}
	g1 << "EOF";

debug();

	/*
	 * Valeurs réelles
	 */

	std::cout << BLUE << "\n*** Valeurs réeelles ***" << NORMAL
			  << " \n mobile.x : " << o.x()
			  << " \n mobile.y : " << o.y()
			  << " \n mobile.vx : " << o.vx()
			  << " \n mobile.vy : " << o.vy()
			  << std::endl;

debug();

	/*
	 * Calcul des matrices C et Y 
	 */
	std::vector<double> angles = Simulator::getAngles(o,obs);
	
	Eigen::MatrixXf C = Estimator::C_Matrix(angles);
	
	Eigen::MatrixXf Y;
	if (i == 0) {
		Y = Estimator::Y_Matrix(angles, *obs.positions());
	} else {
		Y = Estimator::Y_Matrix(angles, *obs.noise_positions());
	}
	
debug();

	Eigen::MatrixXf theoric(4,1);
	theoric(0,0) = o.x();
	theoric(1,0) = o.y();
	theoric(2,0) = o.vx();
	theoric(3,0) = o.vy();

debug();

	/*
	 * Méthode des moindres carrés
	 */

	std::cout << BLUE << "\n*** Méthode des moindres carrés ***" << NORMAL << std::endl;
	Eigen::MatrixXf result = Estimator::inverse(C, Y);
	std::cout << result << std::endl;

debug();

	std::cout << "\n> Prediction de l'erreur" << std::endl;
	Estimator::prediction_error(result, theoric);

	/*
	 * Méthode du gradient conjugué 
	 */
	std::cout << BLUE << "\n*** Méthode du gradient conjugué ***" << NORMAL << std::endl;
	result = Estimator::conjugate_gradient(C, Y);
	std::cout << result << std::endl;

	std::cout << "\n> Prediction de l'erreur" << std::endl;
	Estimator::prediction_error(result, theoric);

	/*
	 * Moindres carrés récursifs
	 */
	std::cout << BLUE << "\n*** Moindres carrés récursifs ***" << NORMAL << std::endl;
	result = Estimator::recursive(angles.size(), C, Y);
	std::cout << result << std::endl;

	std::cout << "\n> Prediction de l'erreur" << std::endl;
	Estimator::prediction_error(result, theoric);
}
}