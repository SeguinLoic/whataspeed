#include "Simulator.h"
#include <Eigen/Dense>
#include <iostream>
#include <cmath>

/**
 * Calcule l'angle formé entre l'objet mobile et l'observateur
 * à un instant t.
 *
 * @return Angle entre objet et observateur à l'instant t
 */
double Simulator::getAngle(Objet obj, Observer obs, int t)
{
	return atan2(obj.y(t) - obs.y(t), obj.x(t) - obs.x(t));
}

/**
 * Calcule les angles formés entre l'objet mobile et l'observateur
 * à tout instant de déplacement.
 *
 * @return Vecteur des angles entre objet mobile et observateur
 */
std::vector<double> Simulator::getAngles(Objet obj, Observer obs)
{
	std::vector<double> angles;
	
	for (int i = 0; i < obs.positions()->cols(); i++)
	{
		angles.push_back(getAngle(obj, obs, i));
	}
	
	return angles;
}
