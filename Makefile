# -----------------------------------------
# File: Makefile
# 
# R. Duperré - V. Sallé - L. Seguin, 2014
# Polytech Nice Sophia - Computer Sciences
# -----------------------------------------

# Compilation

SRC=./src
OBJ=./obj
BIN=./bin
TESTS=./tests

INCL_DIR = ./include
LIB_DIR = ./lib
INCL = -I $(INCL_DIR) -I $(LIB_DIR)/eigen -I $(LIB_DIR)/googletest/include -I $(LIB_DIR)/gnuplot

CXX = g++
CXXFLAGS = -g -Wall -std=c++11 -pthread
CXX_COMPILE = $(CXX) $(CXXFLAGS) $(INCL)

SRC_FILES = $(wildcard $(SRC)/*.cpp)
OBJ_FILES = $(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(SRC_FILES))

TEST_FILES = $(wildcard $(TESTS)/*.cpp)
TEST_OBJ_FILES = $(patsubst $(TESTS)/%.cpp, $(OBJ)/%.o, $(TEST_FILES))

#GTEST_MAIN = lib/googletest/src/gtest_main.cc
#GTEST_MAIN_OBJ = $(OBJ)/gtest_main.o

EXEC = $(BIN)/radar

# Targets

all: checkdir $(EXEC)

checkdir:
	mkdir -p $(OBJ) $(BIN)

$(EXEC): $(OBJ_FILES) $(TEST_OBJ_FILES) # $(GTEST_MAIN_OBJ)
	$(CXX_COMPILE) $^ lib/googletest/libgtest.a -o $@

$(OBJ_FILES): $(OBJ)/%.o : $(SRC)/%.cpp
	$(CXX_COMPILE) -c $< -o $@

$(TEST_OBJ_FILES): $(OBJ)/%.o : $(TESTS)/%.cpp
	$(CXX_COMPILE) -c $< -o $@

$(GTEST_MAIN_OBJ): $(GTEST_MAIN)
	$(CXX_COMPILE) -c $< -o $@

# Clean

clean:
	rm -rf $(OBJ)

mrproper: clean
	rm -rf $(BIN)