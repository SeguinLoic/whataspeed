#ifndef _SIMULATOR_H_
#define _SIMULATOR_H_

#include <vector>
#include "Objet.h"
#include "Observer.h"

class Simulator
{
public:
	// Constructor
	Simulator()
	{}

	// Methods
	double static getAngle(Objet obj, Observer obs, int t);
	std::vector<double> static getAngles(Objet obj, Observer obs);
};

#endif