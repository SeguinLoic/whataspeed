#ifndef _OBSERVER_H_
#define _OBSERVER_H_

#include <cmath>
#include "Objet.h"

class Observer : public Mobile
{
private:
	double _w0, _rayon;
	Eigen::MatrixXf* _noise; // _positions(0) = x(t), _positions(1) = y(t)

public:
	// Constructor
	Observer(double x = 0, double y = 0,
			 double w0 = 2*M_PI/60, double rayon = 1)
		: Mobile(x, y), _w0(w0), _rayon(rayon)
	{
		_noise = new Eigen::MatrixXf(2,0);
	}

	// Getter & setter
	double w0()    const { return _w0; }
	double rayon() const { return _rayon; }
	Eigen::MatrixXf* noise_positions() const { return _noise; }
	double x_noise(int t) const { return (*_noise)(0, t); }
	double y_noise(int t) const { return (*_noise)(1, t); }
	
	void w0(double w0)       { _w0 = w0; }
	void rayon(double rayon) { _rayon = rayon; }

	// Methods
	void   updatePosition(int t);
	double noise();
};

#endif