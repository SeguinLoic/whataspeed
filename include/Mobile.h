#ifndef _MOBILE_H_
#define _MOBILE_H_

#include <Eigen/Dense>

class Mobile
{
protected:
	double _x, _y; // initial positions
	Eigen::MatrixXf* _positions; // _positions(0) = x(t), _positions(1) = y(t)

public:
	// Constructor
	Mobile(double x = 0, double y = 0)
		: _x(x), _y(y)
	{
		_positions = new Eigen::MatrixXf(2,0);
	}

	// Getters
	double x() const { return _x; }
	double y() const { return _y; }
	Eigen::MatrixXf* positions() const { return _positions; }

	double x(int t) const { return (*_positions)(0, t); }
	double y(int t) const { return (*_positions)(1, t); }

	// Methods
	virtual void updatePosition(int t) = 0;
};

#endif