#ifndef _ESTIMATOR_H_
#define _ESTIMATOR_H_

#include <vector>
#include "Objet.h"
#include "Observer.h"

class Estimator
{
public:
	// Constructor
	Estimator()
	{}

	// Methods
	Eigen::MatrixXf static C_Matrix(std::vector<double> angles);
	Eigen::MatrixXf static Y_Matrix(std::vector<double> angles, Eigen::MatrixXf positions);
	Eigen::MatrixXf static inverse(Eigen::MatrixXf C, Eigen::MatrixXf Y);
	Eigen::MatrixXf static conjugate_gradient(Eigen::MatrixXf C, Eigen::MatrixXf Y);
	void static prediction_error(Eigen::MatrixXf computed, Eigen::MatrixXf theoric);
	Eigen::MatrixXf static recursive(int T, Eigen::MatrixXf C, Eigen::MatrixXf Y);
};

#endif