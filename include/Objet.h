#ifndef _OBJET_H_
#define _OBJET_H_

#include "Mobile.h"

class Objet : public Mobile
{
private:
	double _vx, _vy;

public:
	// Constructor
	Objet(double x = 0, double y = 0, double vx = 0, double vy = 0)
		: Mobile(x, y), _vx(vx), _vy(vy)
	{}

	// Getters
	double vx() const { return _vx; }
	double vy() const { return _vy; }

	// Methods
	void updatePosition(int t);
};

#endif