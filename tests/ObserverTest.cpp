#include <iostream>
#include "gtest/gtest.h"
#include "Observer.h"
#include "Simulator.h"
#include "Estimator.h"
#include <cmath>

#define MAX_NONOISE 0.0001
#define MAX_WITHNOISE 1

TEST(ObserverTest, NoNoise)
{ 
	int NBUPDATE_O = 20;
	int NBUPDATE_OBS = 20;

	Objet o(5,5,2,2.5);
	Observer obs(20,20,0.2,50);
	o.updatePosition(NBUPDATE_O);
	obs.updatePosition(NBUPDATE_OBS);

	std::vector<double> angles = Simulator::getAngles(o,obs);
	Eigen::MatrixXf C = Estimator::C_Matrix(angles);
	Eigen::MatrixXf Y = Estimator::Y_Matrix(angles, *obs.positions());

	Eigen::MatrixXf result = Estimator::inverse(C, Y);

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(0,0) + MAX_NONOISE) > o.x() )
				&& ( (Estimator::inverse(C, Y)(0,0) - MAX_NONOISE) < o.x() ));

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(1,0) + MAX_NONOISE) > o.y() )
				&& ( (Estimator::inverse(C, Y)(1,0) - MAX_NONOISE) < o.y() ));

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(2,0) + MAX_NONOISE) > o.vx() )
				&& ( (Estimator::inverse(C, Y)(2,0) - MAX_NONOISE) < o.vx() ));

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(3,0) + MAX_NONOISE) > o.vy() )
				&& ( (Estimator::inverse(C, Y)(3,0) - MAX_NONOISE) < o.vy() ));


	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C2, Y)(0,0) + MAX_NONOISE) > o.x() )
				&& ( (Estimator::conjugate_gradient(C2, Y)(0,0) - MAX_NONOISE) < o.x() ));

	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C2, Y)(1,0) + MAX_NONOISE) > o.y() )
				&& ( (Estimator::conjugate_gradient(C2, Y)(1,0) - MAX_NONOISE) < o.y() ));

	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C2, Y)(2,0) + MAX_NONOISE) > o.vx() )
				&& ( (Estimator::conjugate_gradient(C2, Y)(2,0) - MAX_NONOISE) < o.vx() ));

	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C2, Y)(3,0) + MAX_NONOISE) > o.vy() )
				&& ( (Estimator::conjugate_gradient(C2, Y)(3,0) - MAX_NONOISE) < o.vy() ));
}

TEST(ObserverTest, WithNoise)
{ 
	int NBUPDATE_O = 20;
	int NBUPDATE_OBS = 20;

	Objet o(5,5,2,2.5);
	Observer obs(20,20,0.2,50);
	o.updatePosition(NBUPDATE_O);
	obs.updatePosition(NBUPDATE_OBS);

	std::vector<double> angles = Simulator::getAngles(o,obs);
	Eigen::MatrixXf C = Estimator::C_Matrix(angles);
	Eigen::MatrixXf Y = Estimator::Y_Matrix(angles, *obs.noise_positions());

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(0,0) + MAX_WITHNOISE) > o.x() )
				&& ( (Estimator::inverse(C, Y)(0,0) - MAX_WITHNOISE) < o.x() ));

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(1,0) + MAX_WITHNOISE) > o.y() )
				&& ( (Estimator::inverse(C, Y)(1,0) - MAX_WITHNOISE) < o.y() ));

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(2,0) + MAX_WITHNOISE) > o.vx() )
				&& ( (Estimator::inverse(C, Y)(2,0) - MAX_WITHNOISE) < o.vx() ));

	EXPECT_TRUE(   ( (Estimator::inverse(C, Y)(3,0) + MAX_WITHNOISE) > o.vy() )
				&& ( (Estimator::inverse(C, Y)(3,0) - MAX_WITHNOISE) < o.vy() ));


	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C, Y)(0,0) + MAX_WITHNOISE) > o.x() )
				&& ( (Estimator::conjugate_gradient(C, Y)(0,0) - MAX_WITHNOISE) < o.x() ));

	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C, Y)(1,0) + MAX_WITHNOISE) > o.y() )
				&& ( (Estimator::conjugate_gradient(C, Y)(1,0) - MAX_WITHNOISE) < o.y() ));

	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C, Y)(2,0) + MAX_WITHNOISE) > o.vx() )
				&& ( (Estimator::conjugate_gradient(C, Y)(2,0) - MAX_WITHNOISE) < o.vx() ));

	EXPECT_TRUE(   ( (Estimator::conjugate_gradient(C, Y)(3,0) + MAX_WITHNOISE) > o.vy() )
				&& ( (Estimator::conjugate_gradient(C, Y)(3,0) - MAX_WITHNOISE) < o.vy() ));
}//