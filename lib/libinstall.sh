#!/bin/bash

# -----------------------------------------
# File: libinstall.sh
# 
# R. Duperré - V. Sallé - L. Seguin, 2014
# Polytech Nice Sophia - Computer Sciences
# -----------------------------------------

cd lib

# Eigen
echo "[Eigen]"
hg clone https://bitbucket.org/eigen/eigen/

# GoogleTest
echo -e "\n[GoogleTest]"
svn checkout http://googletest.googlecode.com/svn/trunk/ googletest
cd googletest
	g++ -I include -I . -c src/gtest-all.cc
	ar -rv libgtest.a gtest-all.o
cd ..

# Interface GNUPlot/C++
echo -e "\n[Interface GNUPlot/C++]"
svn checkout http://gnuplot-cpp.googlecode.com/svn/trunk/ gnuplot